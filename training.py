import matplotlib.pyplot as plt # visualization library
import csv # importing the data from the csv files
import numpy as np # used to convert the csv transformed data into a matrix
import os # used to remove the old parameters file

# Constants
ALPHA = 0.3 # learning coefficient
NB_ITER = 600 # number of iteration of gradient descent

# Functions
# Scalling
def average_mileage( ) :
	av_mileage = 0.
	for line in result : # for each mileage
		av_mileage += line[0] # we add it to the total
	av_mileage = av_mileage / nb_examples # and get the average mileage by dividing by the number of them
	return (av_mileage)

def average_price( ) : # same than above
	av_price = 0.
	for line in result :
		av_price += line[1]
	av_price = av_price / nb_examples
	return (av_price)

def scale( ) :
	max_mil = np.amax(result[ : , 0])
	min_mil = np.amin(result[ : , 0])
	
	max_price = np.amax(result[ : , 1])
	min_price = np.amin(result[ : , 1])

	diff_mil = max_mil - min_mil
	diff_price = max_price - min_price

	av_mil = average_mileage()
	av_price = average_price()
	for line in result :
		line[0] = (line[0] - av_mil) / diff_mil
		line[1] = (line[1] - av_price) / diff_price


# Gradient descent
def estimatePrice(mileage) : # estimate the price for a given mileage
	return (np.dot(mileage, parameters)) # matricial multiplication of the input and the parameters

def average_error( ) : # calculating the average error of our model on the dataset
	error = 0 # initial error is null
	for line in result : # for each training example
		x = np.array([1, line[0]]).astype(float) # transform the input as an array for estimatePrice
		diff = estimatePrice(x) - line[1] # get the signed error 
		error += max(diff, -diff) # adding the unsigned error for this example to the global error
	av_error = error / nb_examples # getting the average error by dividing with the number of example in the training set
	return (av_error)

def tmp0( ) :
	error = 0. # initial error is null
	for line in result : # for each training example
		x = np.array([1, line[0]]).astype(float) # transform the input as an array for estimatePrice
		diff = estimatePrice(x) - line[1] # get the signed error
		error += diff
	ret = error / nb_examples 
	return (ret)

def tmp1( ) :
	error = 0 # initial error is null
	for line in result : # for each training example
		x = np.array([1, line[0]]).astype(float) # transform the input as an array for estimatePrice
		diff = estimatePrice(x) - line[1] # get the signed error
		error += diff * line[0]
	ret = error / nb_examples
	return (ret)

# Main
# Retrieving the Data
reader = csv.reader(open('data.csv', 'rb'), delimiter = ',') # opening the file, delimiting with the coma character
x = list(reader) # converting the reader data into a list
x = x[1:] # skipping the first line
result = np.array(x).astype('float') # converting again, into a matrix with float values
nb_examples = len(result)

reader = csv.reader(open('parameters.csv', 'rwb'), delimiter = ',')
parameters = np.array(list(reader)).astype('float') # getting the old parameters and converting them into usefull data
parameters = np.transpose(parameters) #transpose the vector -> matricial product possible

# Feature scalling
scale()

# Gradient descent
err_on_time = [] # list used to display the error at each iteration
for i in range(NB_ITER) :
	err_on_time.append([i, average_error()])
	tmpt0 = ALPHA * tmp0()
	tmpt1 = ALPHA * tmp1()
	parameters[0][0] -= tmpt0
	parameters[1][0] -= tmpt1

# Saving the parameters in the csv file
os.remove('parameters.csv') # deleting the old one
fil = open('parameters.csv', 'w') # creating the new one
fil.write(str(parameters[0][0]) + ',' + str(parameters[1][0])) # writting inside
fil.close()

# Display
err_on_time = np.array(err_on_time) # converting the list into a matrix

plt.title("Training", fontsize = 22, color = 'green') # name of the graphic
plt.xlabel("Iteration", fontsize = 18, color = 'green') # name of the horizontal axis
plt.ylabel("Error", fontsize = 18, color = 'green') # name of the vertical axis
plt.plot(err_on_time[ : , 0], err_on_time[ : , 1], 'b-') # plotting the error function
plt.show() # display the graphic

