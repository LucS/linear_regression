import matplotlib.pyplot as plt # visualization library
import csv # importing the data from the csv files
import numpy as np # used to convert the csv transformed data into a matrix
import sys # for inputs

# Functions
def estimatePrice(mileage) : # estimate the price for a given mileage
	return (parameters[0][0] + mileage * parameters[1][0]) # matricial multiplication of the input and the parameters

# Scalling
def average_mileage( ) :
	av_mileage = 0.
	for line in result : # for each mileage
		av_mileage += line[0] # we add it to the total
	av_mileage = av_mileage / nb_examples # and get the average mileage by dividing by the number of them
	return (av_mileage)

def average_price( ) : # same than above
	av_price = 0.
	for line in result :
		av_price += line[1]
	av_price = av_price / nb_examples
	return (av_price)

def scale(inp) :
	max_mil = np.amax(result[ : , 0])
	min_mil = np.amin(result[ : , 0])
	
	max_price = np.amax(result[ : , 1])
	min_price = np.amin(result[ : , 1])

	diff_mil = max_mil - min_mil
	diff_price = max_price - min_price

	av_mil = average_mileage()
	av_price = average_price()
	for line in result :
		line[0] = (line[0] - av_mil) / diff_mil
		line[1] = (line[1] - av_price) / diff_price
	inp = (inp - av_mil) / diff_mil
	return ([inp, diff_price , av_price])

# Main
# Retrieving the input
try : # trying to get the input
	inp = np.array(sys.argv[1]).astype('float')
except Exception : # if no input, or too much, leaving
	exit()

# Retrieving the data
reader = csv.reader(open('data.csv', 'rb'), delimiter = ',') # opening the file, delimiting with the coma character
x = list(reader) # converting the reader data into a list
x = x[1 : ] # skipping the first line
result = np.array(x).astype('float') # converting again, into a matrix with float values

reader = csv.reader(open('parameters.csv', 'rwb'), delimiter = ',')
parameters = np.array(list(reader)).astype('float') # getting the old parameters and converting them into usefull data
parameters = np.transpose(parameters) #transpose the vector -> matricial product possible

# Feature scalling
nb_examples = len(result) # the size of the dataset
res = scale(inp) # scalling the dataset and the input
inp = res[0]
ans = estimatePrice(inp) # the supposed price of the input

# Printting the hypothetic result for the input
print (ans * res[1] + res[2]) # Of course, the answer is scale, we have to reverse-engineering the answer to get the real price

# Plotting
x = np.arange(-0.5, 1, 0.01) # scope of our hypothesis
plt.title("Prediction", fontsize = 22, color = 'green') # title of the graphic
plt.xlabel("Mileage", fontsize = 18, color = 'green') # title of the horizontal axis
plt.ylabel("Price", fontsize = 18, color = 'green') # title of the vertical axis
p1, = plt.plot(result[ : , 0], result[ : , 1], 'ro', markersize = 12) # creating the graph, data in red dots 'ro'
p2, = plt.plot(x, estimatePrice(x), 'g-') # plotting the hypothesis function in a green line 'g-'
p3, = plt.plot(inp, ans, "bo", markersize = 8) # plotting the input mileage, associated to the estimated price for this mileage
plt.legend([p1, p2, p3], ["Dataset", "Hypothesis", "Input"]) # for a legend
plt.show() # display on screen