# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Help 42 students getting an idea of what the linear_regression project
  could look like.
* V1

### How do I get set up? ###

* Summary of set up :

  - If you dont have all the librairies, download them with
    "pip install --user [$libname]"

* How to use it :
  - "python predict.py [$mileage]" will compile and launch the predict.py 
    program, it will open parameters.csv to get the parameters of the 
    hypothesis and data.csv to retrieve the dataset. First, it will 
    evaluate the hypothetic price associated to the $mileage input and
    print it in the console, then it will display on screen a graph
    composed with the dataset, the hypothesis and the input.
  - "python training.py" will compile and launch the training.py program,
    it will open the data.csv file to get the dataset and use linear
    regression on it with the gradient descent method to get the optimum 
    parameters for our hypothesis and save the new ones in the 
    parameters.csv file.
    Note : if you relaunch training.py, it will start the linear regression
    with the old values of the parameters that were saved in the
    parameters.csv file, initially (0,0).

### Contributors ###

* Made by lsalomme